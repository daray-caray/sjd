<?php
//Classe genérica para conexão com o Banco, banco abstraído em PDO.

class crud_geral
{

  protected static $tabela ;
  protected static $campo_id;

  function __construct()
  {
    global $path;
    $db_string = "sqlite:".$path."database/".static::$database; //To usando o front agora, mas sei lá, melhor fazer uma chave

    $this->obj_db = new PDO($db_string);
    $this->obj_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $this->conn = new StdClass();
    $this->conn->tabela = static::$tabela;
    $this->conn->campo_id = static::$campo_id;

    $this->dados = new StdClass();
    $this->lista_todos();
    $this->zera_properties();
    $this->colunas();
    $this->num_registros();
  }

  function zera_properties()
  {
    foreach ($this->dados as $key => $value) {
        // echo $this->dados->$key = NULL; //DEBUG
    }
  }

  function lista_todos()
  {
    $tabela = $this->conn->tabela;
    $return = $this->obj_db->query("select * from $tabela");
    $result = $return->fetchAll(PDO::FETCH_ASSOC);
    // print_r($result); //passar isso aqui para o objeto $this->dados  //DEBUG

    $this->todos = new StdClass();
    $this->todos = $result;

    return $result; //SAIDA DUAL
  }

  function num_registros()
  {
    $tabela = $this->conn->tabela;
    $return = $this->obj_db->query("select count(*) from $tabela");
    $result = $return->fetchAll(PDO::FETCH_ASSOC);
    // print_r($result); //passar isso aqui para o objeto $this->dados  //DEBUG

    $this->num_registros = $result;

    return $result; //SAIDA DUAL
  }

  function seleciona($id)
  {
    if(!isset($id))
    {
      return false;
      exit;
    }
    $tabela = $this->conn->tabela;
    $campo_id = $this->conn->campo_id;

    $this->zera_properties();

    try {
      $return = $this->obj_db->query("select * from $tabela where $campo_id = '$id'");

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
    }

    //echo  "select * from $tabela where $campo_id = '$id'"; // DEBUG
    $result = $return->fetch(PDO::FETCH_ASSOC);

    // var_dump($return); //DEBUG

    foreach ($result as $key => $value) {
       $this->dados->$key = $value;       //Escreve as vars no objeto
    }
    //var_dump($this->dados); //DEBUG
    return $result; //Colocar na documentaao que é funcao DUAL (array e objeto)
  }

  function deleta($id)
  {
    if(!isset($id))
    {
      return false;
      exit;
    }
    $tabela = $this->conn->tabela;
    $campo_id = $this->conn->campo_id;
    try {
      $return = $this->obj_db->exec("delete from $tabela where $campo_id = '$id'");

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
        exit;
    }
    return $return;
  }

  function insere()
  {
    $tabela = $this->conn->tabela;

    $sql_query =
    "
    insert into $tabela
    (";

    foreach ($this->dados as $key => $value) {
    $sql_query .= $key . ',
    ';
      }

    $sql_query .= "
    ultimousuario,
    ultimaalteracao)
    values
    (
    ";
    foreach ($this->dados as $key => $value) {
    $sql_query .= $this->obj_db->quote($value). ","
    ;
      }

    $sql_query .= "
    'TXTultimousuario',
    '".time()."')
    ";

    try {
      $this->obj_db->exec($sql_query);

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        echo "$sql_query<br>";
        var_dump($e->getMessage());
        exit;
    }
    //  echo $sql_query; //DEBUG

    return $this->obj_db->lastInsertId();
  }

  function atualiza($id) //Avisar que o front-end tem que mandar os dados todos, nao só a alteracao
  {
    $tabela = $this->conn->tabela;
    $campo_id = $this->conn->campo_id;

    $sql_query =
    "
    update $tabela set
    ";

    foreach ($this->dados as $key => $value) {
    $sql_query .= $key . ' = ' .$value . ', ';
      }

    $sql_query .= "
    ultimousuario = 'TXTultimousuario' ,
    ultimaalteracao = '".time()."'

    where $campo_id = '$id'
    ";
    try {
      $this->obj_db->exec($sql_query);

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        echo "$sql_query<br>";
        var_dump($e->getMessage());
        exit;
    }

    return true;
  }

  function colunas()
  {

    $tabela = $this->conn->tabela;

    $sql_query = "
    pragma table_info('$tabela')
    ";
    try {

      $return = $this->obj_db->query($sql_query);
      $result = $return->fetchAll(PDO::FETCH_ASSOC);

      $this->colunas = $result;

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
        exit;
    }


  }



}
  ?>
