<?php

class andamentos extends crud_geral
{
  protected static $tabela = 'jur_andamentos';
  protected static $campo_id = 'id_andamento';
  protected static $database = 'sjd_main.db';
}
class clientes extends crud_geral
{
  protected static $tabela = 'ger_clientes';
  protected static $campo_id = 'id_cliente';
  protected static $database = 'sjd_main.db';
}
class lancamentos extends crud_geral
{
  protected static $tabela = 'fin_lancamentos';
  protected static $database = 'sjd_main.db';
  protected static $campo_id = 'id_lancamento';
}
class documentos extends crud_geral
{
  protected static $tabela = 'jur_documentos';
  protected static $database = 'sjd_main.db';
  protected static $campo_id = 'id_documento';
}
class fornecedor extends crud_geral
{
  protected static $tabela = 'ger_fornecedores';
  protected static $database = 'sjd_main.db';
  protected static $campo_id = 'id_fornecedor';
}
class login extends crud_geral
{
  protected static $tabela = 'ger_login';
  protected static $database = 'sjd_main.db';
  protected static $campo_id = 'id_login';
}
class processos extends crud_geral
{
  protected static $tabela = 'jur_processos';
  protected static $database = 'sjd_main.db';
  protected static $campo_id = 'id_processo';
}

class front_conteudo extends crud_geral
{
  protected static $tabela = 'front_conteudo';
  protected static $database = 'sjd_front.db';
  protected static $campo_id = 'id_conteudo';
}

class front_usuarios extends crud_geral
{
  protected static $tabela = 'admin_usuarios';
  protected static $database = 'sjd_front.db';
  protected static $campo_id = 'id_usuario';
}

class front_idiomas extends crud_geral
{
  protected static $tabela = 'front_idiomas';
  protected static $database = 'sjd_front.db';
  protected static $campo_id = 'id_idioma';
}

?>
