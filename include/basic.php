<?php


function redirect($url)
{
    if (!headers_sent())
    {
        header('Location: '.$url);
        exit;
        }
    else
        {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
    }
}


//ALBERTO, movi a função para o arquivo BASIC, motivos óbvios.
function montaTabela($objeto,$visivel,$limit = '') {
  $objeto_tabela = new $objeto;


  if($objeto == 'clientes') $objeto_processos = new processos;  //Nem de longe o melhor a se fazer


  $tabela = "
    <table class='table table-striped'>
      <thead>
        <tr>
        ";

        if($objeto == 'clientes') $tabela .= "<th> PROCESSOS </th>";


  foreach ($objeto_tabela->colunas as $colunas) {
    // global $visivel;

    if(array_search($colunas['name'],$visivel)) {
    $tabela .= "<th>".strtoupper($colunas['name'])."</th>";
    }
  }
  $tabela .= "<th class='text-center'>EDITAR/DELETAR</th>";

  $tabela .= "
    </tr>
      </thead>
        <tbody>
        ";

  foreach ($objeto_tabela->todos as $key => $dados) {

  $nome_parametro = substr($objeto,0,-1);
  if($objeto == 'clientes') $nome_parametro = 'id_cliente'; //pegando o id para passar por parametro
  if($objeto == 'processos') $nome_parametro = 'id_processo';

// if(1==1)
if($limit != '') // INICIO DO LIMITADOR de LIMIT
{
  if($limit == $dados["link_id_cliente"]) // INICIO DO LIMITADOR
  {
      $tabela .= "<tr>";
      foreach ($dados as $key => $value) {
        if(array_search($key,$visivel)) {
        $tabela .= "<td>".$value."</td>";
      }
      }
      $tabela .= "<td class='text-center'><a href='edit_".$objeto.".php?".$nome_parametro."=".$dados[$nome_parametro]."'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;<a href='#'><i class='fa fa-trash'></i></a></td>";

      $tabela .= "</tr>";

    } // Fim do IF LIMITADOR
} //FIM DO LIMITADOR de LIMIT
else {    // NAO CONSIGO NEM EXPRESSAR O QUANTO ESTOU PUTO COM ISSO -- ARRUMAR URGENTE APOS A ENTREGA

  $tabela .= "<tr>";

  $num_processos = 0;

if($objeto == 'clientes'){
    foreach ($objeto_processos->todos as $key => $value) {
      if($dados["id_cliente"] == $value[link_id_cliente]) $num_processos++ ;  // eu conto o número de processos para exibir na tabela
    }
}


  if($objeto == 'clientes') $tabela .=  "<td> <a href=list_processos.php?limit=".$dados["id_cliente"]."> PROCESSOS ($num_processos) </a></td>";
  foreach ($dados as $key => $value) {
    if(array_search($key,$visivel)) {
    $tabela .= "<td>".$value."</td>";
  }
  }

  $tabela .= "<td class='text-center'><a href='edit_".$objeto.".php?".$nome_parametro."=".$dados[$nome_parametro]."'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;<a href='#'><i class='fa fa-trash'></i></a></td>";

  $tabela .= "</tr>";


}


} // Fim do Foreach de Linhas

  $tabela .= "
    </tbody>
  </table>
";

echo $tabela;
}

function montaForm($objeto,$visivel,$labels,$types,$action, $values_form = NULL) {
  $objeto_tabela = new $objeto;

  // var_dump($visivel); // DEBUG

// echo '<pre>';
// var_dump($objeto_tabela->colunas);
// exit;


$form = '
<form enctype="multipart/form-data" action="'.$action.'" method="post">
  <div class="col-md-12">
';

foreach ($objeto_tabela->colunas as $key => $value) {
  if(array_search($value["name"],$visivel)) {
  // echo '<pre>'; //DEBUG
  // var_dump($value);
  // exit;

    if($types[$value["name"]] == 'textarea') {
      $input = '<textarea name="'.$key.'">'.$values_form[$value["name"]].'</textarea>';
    } else {
      $input = '<input type="'.$types[$value["name"]].'" class="col-md-6" placeholder="'.$labels[$value["name"]].'" name="'.$value["name"].'" value="'.$values_form[$value["name"]].'">';
    }


    // $form .= $value["name"];
    $form .= '
  <div class="control-group">
    <label class="control-label bolder blue">'.$labels[$value["name"]].'</label><br>
    '.$input.'
  </div>
  <br><br><br><br>
';

  }
}

$form .= '
<div style="margin-top: 10px;">
  <button class="btn" type="submit">Salvar</button>
</div>
</div>
</form>';

echo $form;
}


?>
