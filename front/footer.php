<footer id="colophon" class="site-footer" role="contentinfo">
	<div id="supplementary">
		<div id="footer-sidebar" class="footer-sidebar widget-area" role="complementary">
			<aside id="image-2" class="widget widget_image">
				<h1 class="widget-title">Desenvolvimento e Criação:</h1>
				<div class="jetpack-image-container">
					<figure  class="wp-caption alignnone">
						<a target="_blank" href="http://www.daray.com.br"><img src="http://sa2.com.br/daray/wp-content/uploads/2015/11/logodaray.png" alt="Daray Web!" title="Daray Web!" /></a>
					<figcaption class="wp-caption-text">Daray Web!</figcaption>
					</figure>
				</div>
			</aside>
		</div>
	</div>

	<div class="site-info">
		<a href="http://daray.com.br/">Orgulhosamente desenvolvido por Daray Web</a>
	</div>
</footer>
</div>

<div style="display:none">
</div>
<link rel='stylesheet' id='contact-info-map-css-css'  href='css/contato-info-map.css' type='text/css' media='all' />
<script type='text/javascript' src='js/device-jetpack.js'></script>
<script type='text/javascript' src='js/gravatar.js'></script>
<script type='text/javascript'>

var WPGroHo = {"my_hash":""};

</script>
<script type='text/javascript' src='js/jetpackwp.js'></script>
<script type='text/javascript' src='js/mansory.js'></script>
<script type='text/javascript' src='js/jquery-mansory.js'></script>
<script type='text/javascript'>

var featuredSliderDefaults = {"prevText":"Anterior","nextText":"Pr\u00f3ximo"};

</script>
<script type='text/javascript' src='js/slider.js'></script>
<script type='text/javascript' src='js/functions.js'></script>
<script type='text/javascript' src='js/embed.js'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?sensor=false&#038;ver=4.4.10'></script>
<script type='text/javascript' src='js/contato-info-map_2.js'></script>
<script type='text/javascript' src='js/stats.js' async defer></script>
<script type='text/javascript'>
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:3.9.7',blog:'107870861',post:'2',tz:'0',srv:'sa2.com.br'} ]);
	_stq.push([ 'clickTrackerInit', '107870861', '2' ]);
</script>
</body>
</html>
