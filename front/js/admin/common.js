num_input_arquivos = '4';

$( document ).ready(function() {
	
	$('#addarquivos').click(function(){
		$('#arquivos').append('<input type="file" name="arquivo-'+num_input_arquivos++ +'" class="col-sm-12"><br><br>');
	});

	
	$('#nome_campanha').blur(function(){
		var dados = new FormData();
		var input = document.getElementById('nome_campanha');
		dados.append('nome_campanha', input.value);

		$.ajax({
	        type: "POST",
			url: baseUrl+"sistema/Campanhas/validaUrl",
			data: dados,
			processData: false,
			contentType: false,
	        success: function(data) {
	        	$('#url_campanha').attr('value', baseUrl+'uploads/campanhas/'+data);
	        	$('#arquivos_campanha').attr('value', baseUrl+'uploads/campanhas/'+data+'.zip');
	        },
	        error: function() {
	        	alert('Erro ao validar URL');
	        }
    	});
	});

	$('#thumb_campanha_file').change(function(){
		var dados = new FormData();
		var input = document.getElementById('thumb_campanha_file');
		dados.append('thumb_campanha_file', input.files[0]);

		$.ajax({
	        type: "POST",
			url: baseUrl+"sistema/Campanhas/validaUrlThumb",
			data: dados,
			processData: false,
			contentType: false,
	        success: function(data) {
	        	$('#thumb_campanha').attr('value', data);
	        },
	        error: function() {
	        	alert('Erro ao validar URL');
	        }
    	});
	});

	$('#apresentacao_campanha_file').change(function(){
		var dados = new FormData();
		var input = document.getElementById('apresentacao_campanha_file');
		dados.append('apresentacao_campanha_file', input.files[0]);
		console.log(input.files[0]);

		$.ajax({
	        type: "POST",
			url: baseUrl+"sistema/Campanhas/validaUrlApresentacao",
			data: dados,
			processData: false,
			contentType: false,
	        success: function(data) {
	        	$('#apresentacao_campanha').attr('value', data);
	        },
	        error: function() {
	        	alert('Erro ao validar URL');
	        },
	        timeout: 150000
    	});
	});

	$("#form_campanhas").submit(function(e){
	    e.preventDefault();
	    var input = document.getElementById('form_campanhas');
	    var dados = new FormData(input);
	    dados.append('arquivos_preview_campanha', num_input_arquivos);

		$.ajax({
			type: "POST",
			url: baseUrl+"sistema/Campanhas/salvaCampanha",
			data: dados,
			processData: false,
			contentType: false,
			success: function(data) {
				console.log(data);
				alert("Salvo com sucesso!");
				$('.limpar_input').val('');
			},
			error: function(data) {
				alert("Erro ao salvar!");
			}
		});
	});
});