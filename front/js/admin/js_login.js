$( document ).ready(function() {

	
	/** 
	 *Função que verifica se o campo e-mail está vazio
	 *#usuario_login
	 *@author Alberto Aguiar
	 *@access public
	 *@param
	 *@return void 
	*/
	$("#alert_error").hide();
	$('#alert_error_form').hide();
	$("#usuario_login").blur(function(){
		value_usuario_input = $("#usuario_login").val();
		
		if(value_usuario_input == '') {
			$("#alert_error").show();
		} else {
			$("#alert_error").hide();
		}
	});

	/** 
	 *Função que verifica se o campo senha está vazio
	 *#senha_login
	 *@author Alberto Aguiar
	 *@access public
	 *@param
	 *@return void 
	*/
	$("#alert_error_senha").hide();
	$("#senha_login").blur(function(){
		value_senha_input = $("#senha_login").val();
		
		if(value_senha_input == '') {
			$("#alert_error_senha").show();
		} else {
			$("#alert_error_senha").hide();
		}
	});

	$("#form_login").submit(function(e){
	    e.preventDefault();
	    var input = document.getElementById('form_login');
	    var dados = new FormData(input);
	    console.log(dados);

		$.ajax({
			type: "POST",
			url: baseUrl+"sistema/VerifyLogin",
			data: dados,
			processData: false,
			contentType: false,
			success: function(data) {
				verificaeenvia(data);
			},
			error: function(data) {
				$('#alert_error_form').append('Usuário ou senha inválidos!');
			}
		});
	});

	function verificaeenvia(data) {
		if(data == 'FALSE') {
			$('#alert_error_form').show();
		} else {
			window.location="insercao-campanhas";
		}
	}
});