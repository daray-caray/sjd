$.validator.addMethod("exactlenght", function(value, element, param) {
    return this.optional(element) || (value != "" ? (value.length == param) : true);
});

$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value.search(/[^a-z]/i) == -1;
});

$.validator.addMethod("alphaNumeric", function(value, element) {
    return this.optional(element) || value.search(/[^a-z0-9]/i) == -1;
});

$.validator.addMethod("alphaDash", function(value, element) {
    return this.optional(element) || value.search(/[^\w-]/i) == -1;
});

$.validator.addMethod("integer", function(value, element) {
    return this.optional(element) || !(isNaN(value) || value.indexOf(".") > -1)
});

$.validator.addMethod("isNatural", function(value, element) {
    return this.optional(element) || (!isNaN(value) && value >= 0)
});

$.validator.addMethod("isNaturalNoZero", function(value, element) {
    return this.optional(element) || (!isNaN(value) && value > 0)
});

$.validator.addMethod("ip", function(value, element) {
    if (this.optional(element))
        return "dependency-mismatch";

    var segs = value.split(".");
    for (var i in segs)
        if(segs[i].length>3 || segs[i]>255 || segs[i].search(/\D/)>-1)
            return false;
    return true;
});

$.validator.addMethod("base64", function(value, element) {
    return this.optional(element) || value.search(/[^a-zA-Z0-9\/\+=]/) == -1;
});

$.validator.addMethod("cpf", function(value, element) {

    //Recebe o valor
    value = $.trim(value);

	//Trata, retirando os caracteres especiais
	value = value.replace('.','');
	value = value.replace('.','');
	cpf   = value.replace('-','');

    //Adiciona zeros a esquerda (se necessário)
    while(cpf.length < 11) cpf = "0"+ cpf;

    //Verifica se não é uma string seguida
    if(cpf == '00000000000' || cpf == '11111111111' || cpf == '22222222222' || cpf == '33333333333' || cpf == '44444444444' || cpf == '55555555555' || cpf == '66666666666' || cpf == '77777777777' || cpf == '88888888888' || cpf == '99999999999')
        return false

	//Algoritmo de validação
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	var a = [];
	var b = new Number;
	var c = 11;
	for (i=0; i<11; i++){
		a[i] = cpf.charAt(i);
		if (i < 9) b += (a[i] * --c);
	}
	if ((x = b % 11) < 2)
        a[9] = 0;
    else
        a[9] = 11-x;
	b = 0;
	c = 11;
	for (y=0; y<10; y++)
        b += (a[y] * c--);
	if ((x = b % 11) < 2)
        a[10] = 0;
    else
        a[10] = 11-x;
	if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg))
        return false;

    //Tudo ok
    return true;
});

$.validator.addMethod("cnpj", function(value, element) {

    //Limpa o valor recebido
    cnpj = $.trim(value);
    cnpj = cnpj.replace('/','');
    cnpj = cnpj.replace('.','');
    cnpj = cnpj.replace('.','');
    cnpj = cnpj.replace('-','');

    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;

    if (cnpj.length < 14 && cnpj.length < 15){
        return false;
    }
    for (i = 0; i < cnpj.length - 1; i++){
        if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
            digitos_iguais = 0;
            break;
        }
    }

    if (!digitos_iguais){
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (i = tamanho; i >= 1; i--){
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2){
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)){
            return false;
        }
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--){
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2){
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)){
            return false;
        }
        return true;
    }
    else
        return false;
});

$.validator.addMethod("callback", function(value, element, param) {
    if (this.optional(element))
        return "dependency-mismatch";

    //Busca o elemento anterior
    var previous = this.previousValue(element);

    //Verifica se o valor foi trocado
    if (previous.old !== value) {

        //Monta os dados
        var data = {};
        if(param.data) {
            //Verifica se foi passado algum id de campo
            if($("#"+param.data).val())
                data[param.data] = $("#"+param.data).val();
            else
                data[param.data] = "";
        }

        //Monta a str, o flag ajax_in_use e seta o flag para não salvar a url
        data["str"]         = value;
        data["ajax_in_use"] = true;
        data["noSaveUrl"]   = true;

        previous.old = value;
        var validator = this;
        this.startRequest(element);
        //Executa a chamada a página passada no param
        $.ajax({
            url: param.url,
            type: "post",
            dataType: "json",
            data: data,
            success: function(response) {
                if (!response) {
                    //Em caso de false, exibe os erros
                    var errors = {};
                    errors[element.name] =  response || validator.defaultMessage(element, "callback");
                    validator.showErrors(errors);
                } else {
                    var submitted = validator.formSubmitted;
                    validator.prepareElement(element);
                    validator.formSubmitted = submitted;
                    validator.successList.push(element);
                    validator.showErrors();
                }
                previous.valid = response;
                validator.stopRequest(element, response);
            }
        });
        //Segura o validator
        return "pending";
    }
    else if(this.pending[element.name])
       //Segura o validator
       return "pending";

    //Retorna a validação
    return previous.valid;

});
