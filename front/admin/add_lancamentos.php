<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Lançamentos";

include "header.php";

echo "<a class='btn pull-right' href='list_lancamentos.php'>Voltar</a>";

$visivel = array('','id_lancamento','link_id_fornecedor','link_id_fornecedor','vencimento','tipo','datadoc', 'vlrdoc', 'desconto', 'moramulta', 'vlrcob', 'liquidado', 'obs', 'pagamento_data', 'baixado', 'motivo_baixa');  // ALBERTO aqui a gente deixa ele ver o que a gente quiser. O PRIMEIRO SAI CAGADO, eu já vi isso em algum lugar, mas sem tempo de olhar o pq novamente.
$labels = array('' => '',
'id_lancamento'  => '',
'link_id_fornecedor'   => '',
'link_id_fornecedor'  => '',
'vencimento'  => 'Vencimento',
'tipo'  => 'Tipo',
'datadoc'  => 'Data do documento',
'vlrdoc' => 'Valor do documento',
'desconto'=> 'Desconto',
'moramulta'=> 'Multa de mora',
'vlrcob'=> 'Valor da cobrança',
'liquidado'=> 'Liquidado',
'obs'=> 'Observações',
'pagamento_data'=> 'Data de pagamento',
'baixado'=> 'Baixado',
'motivo_baixa'=> 'Motivo da baixa',
);
$types = array('' => '',
'id_lancamento'  => 'hidden',
'link_id_fornecedor'   => 'hidden',
'link_id_fornecedor'  => 'hidden',
'vencimento'  => 'date',
'tipo'  => 'text',
'datadoc'  => 'date',
'vlrdoc' =>'text',
'desconto'=>'text',
'moramulta'=>'text',
'vlrcob'=>'text',
'liquidado'=>'checkbox',
'obs'=>'textarea',
'pagamento_data'=>'date',
'baixado'=>'checkbox',
'motivo_baixa'=>'text',
);
$action = 'ACTION_COLOCAR';
montaForm('lancamentos',$visivel,$labels,$types,$action);

include "footer.php";

//$visivel = array('','link_id_cliente','forum','partes','vara','controle','pvencimentoente', 'data');

?>
