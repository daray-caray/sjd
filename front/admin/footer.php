</div>
</div>
<div class="vspace-12-sm"></div>
          </div><!-- /.row -->

          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->
<div class="footer">
  <div class="footer-inner">
    <div class="footer-content">
      <span class="bigger-120">
        <span class="bolder">Daray</span>
        - Copyright &copy;
      </span>

      &nbsp; &nbsp;
      <span class="action-buttons">
        <a href="#">
          <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
        </a>

        <a href="#">
          <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
        </a>
      </span>
    </div>
  </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
  <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="../js/admin/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="../js/admin/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
if('ontouchstart' in document.documentElement) document.write("<script src='../js/admin/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="../js/admin/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="../js/admin/excanvas.min.js"></script>
<![endif]-->
<script src="../js/admin/jquery-ui.custom.min.js"></script>
<script src="../js/admin/jquery.ui.touch-punch.min.js"></script>
<script src="../js/admin/jquery.easypiechart.min.js"></script>
<script src="../js/admin/jquery.sparkline.index.min.js"></script>
<script src="../js/admin/jquery.flot.min.js"></script>
<script src="../js/admin/jquery.flot.pie.min.js"></script>
<script src="../js/admin/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->
<script src="../js/admin/ace-elements.min.js"></script>
<script src="../js/admin/ace.min.js"></script>

<!-- inline scripts related to this page -->
</body>
</html>
