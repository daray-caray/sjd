<!DOCTYPE html>
<html lang="en">
  <head>

    <style>
      #sortable img {
        width: 30%;
        margin: 10px 0;
      }
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>.:::Administração:::.</title>

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="../css/admin/bootstrap.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="../css/admin/fonts.googleapis.com.css" />
    <link rel="stylesheet" href="../css/admin/style.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="../css/admin/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="../css/admin/ace-part2.min.css" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="../css/admin/ace-skins.min.css" />
    <link rel="stylesheet" href="../css/admin/ace-rtl.min.css" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="../css/admin/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="../js/admin/ace-extra.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/admin/common.js"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="../js/admin/html5shiv.min.js"></script>
    <script src="../js/admin/respond.min.js"></script>
    <![endif]-->

    <?php
      if(isset($script)) {
        echo "<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script><script>tinymce.init({ selector:'textarea', file_picker_types: 'file image media'});</script>";
      }
    ?>
  </head>

  <body class="no-skin">
    <div id="navbar" class="navbar navbar-default ace-save-state">
      <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
          <span class="sr-only">Toggle sidebar</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
          <a href="" class="navbar-brand">
            <small>
              Olá, <?php
              if(!isset($_SESSION)) session_start();
              echo $_SESSION["username"]; ?>
            </small>
          </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
          <ul class="nav ace-nav">

            <li class="light-blue dropdown-modal">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <span class="user-info">
                 <i class="ace-icon fa fa-user bigger-200"></i>
                <i class="ace-icon fa fa-caret-down"></i>
              </a>

              <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                <li class="divider"></li>
                <!-- <li>
                  <a href="logout.php">
                    <i class="ace-icon fa fa-key"></i>
                    Mudar senha
                  </a>
                </li> -->
                <li>
                  <a href="logout.php">
                    <i class="ace-icon fa fa-power-off"></i>
                    Deslogar
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
      </script>

      <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <ul class="nav nav-list">
          <li class="active">
            <a href="">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Administração </span>
            </a>

            <b class="arrow"></b>
          </li>

          <li>
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text"> Escritório </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
              <li class="">
                <a href="list_processos.php">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Processos
                </a>

                <b class="arrow"></b>
              </li>
              <li class="">
                <a href="list_clientes.php">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Clientes
                </a>

                <b class="arrow"></b>
              </li>
              <!-- <li class="">
                <a href="list_andamentos.php">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Andamentos
                </a>

                <b class="arrow"></b>
              </li>
              <li class="">
                <a href="list_lancamentos.php">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Lançamentos
                </a>

                <b class="arrow"></b>
              </li> -->
            </ul>
          </li>
<!-- Inicio de SITE -->
          <li>
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text"> Site </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

              <li>
                <a href="#" class="dropdown-toggle">
                  <i class="menu-icon fa fa-globe"></i>
                  <span class="menu-text">
                    <?php
                    $objeto_idiomas = new front_idiomas;
                    echo $objeto_idiomas->seleciona(1)['nome'];
                     ?>
                  </span>

                  <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                  <li class="">
                    <a href="edit_conteudo.php?id=99999&lid=1">
                      <i class="menu-icon fa fa-plus"></i>
                      <strong>Nova Página</strong>

                      <b class="arrow"></b>
                    </a>
                  </li>

                  <li class="">
                    <a href="edit_conteudo.php?id=0&lid=1">
                      <i class="menu-icon fa fa-star-o"></i>
                      Home

                      <b class="arrow"></b>
                    </a>
                  </li>

                  <?php

                  $objeto = new front_conteudo;

                  // var_dump($objeto->todos); //DEBUG

                  foreach ($objeto->todos as $key => $value) {
                    if ($key == 0)
                    {
                    $key = 1;
                    continue;
                    }
                      $menu_item = '<li class="">
                                      <a href="edit_conteudo.php?id='.$value['id_conteudo'].'&lid=1">
                                        <i class="menu-icon fa fa-globe"></i> ';

                      $menu_item .= $value["title_idioma_1"].'<br>';
                      $menu_item .= '</a>

                      <b class="arrow"></b>
                    </li>';

                    echo $menu_item;
                  }


                  ?>
                </ul>
              </li>

              <li>
                <a href="#" class="dropdown-toggle">
                  <i class="menu-icon fa fa-globe"></i>
                  <span class="menu-text">
                    <?php
                    echo $objeto_idiomas->seleciona(2)['nome'];
                    ?>
                  </span>

                  <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                  <li class="">
                    <a href="edit_conteudo.php?id=99999&lid=2">
                      <i class="menu-icon fa fa-plus"></i>
                      <strong>Nova Página</strong>

                      <b class="arrow"></b>
                    </a>
                  </li>

                  <li class="">
                    <a href="edit_conteudo.php?id=0&lid=2">
                      <i class="menu-icon fa fa-star-o "></i>
                      Home

                      <b class="arrow"></b>
                    </a>
                  </li>
                  <?php
                  // $objeto = new front_conteudo;

                  // var_dump($objeto->todos); //DEBUG

                  foreach ($objeto->todos as $key => $value) {
                    if ($key == 0)
                    {
                    $key = 1;
                    continue;
                    }$menu_item = '<li class="">
                                      <a href="edit_conteudo.php?id='.$value['id_conteudo'].'&lid=2">
                                        <i class="menu-icon fa fa-globe"></i> ';

                      $menu_item .= $value["title_idioma_2"].'<br>';
                      $menu_item .= '</a>

                      <b class="arrow"></b>
                    </li>';

                    echo $menu_item;
                  }

                  ?>
                </ul>
              </li>

              <li>
                <a href="#" class="dropdown-toggle">
                  <i class="menu-icon fa fa-globe"></i>
                  <span class="menu-text">
                    <?php
                    echo $objeto_idiomas->seleciona(3)['nome'];
                    ?>
                  </span>

                  <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                  <li class="">
                    <a href="edit_conteudo.php?id=99999&lid=3">
                      <i class="menu-icon fa fa-plus"></i>
                      <strong>Nova Página</strong>

                      <b class="arrow"></b>
                    </a>
                  </li>

                  <li class="">
                    <a href="edit_conteudo.php?id=0&lid=3">
                      <i class="menu-icon fa fa-star-o"></i>
                      HOME

                      <b class="arrow"></b>
                    </a>
                  </li>

                  <?php
                  // $objeto = new front_conteudo;

                  // var_dump($objeto->todos); //DEBUG

                  foreach ($objeto->todos as $key => $value) {
                    if ($key == 0)
                    {
                    $key = 1;
                    continue;
                    }
                      $menu_item = '<li class="">
                                      <a href="edit_conteudo.php?id='.$value['id_conteudo'].'&lid=3">
                                        <i class="menu-icon fa fa-globe"></i> ';

                      $menu_item .= $value["title_idioma_3"].'<br>';
                      $menu_item .= '</a>

                      <b class="arrow"></b>
                    </li>';



                    echo $menu_item;
                  }


                  ?>
                </ul>
              </li>
            </ul>
          </li>
<!-- FIM DO ITEM SITE -->
<!-- Inicio de USUARIOS -->
          <li>
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa  fa-key"></i>
              <span class="menu-text"> Usuários </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">


              <li class="">
              <a href="edit_users.php?id=0">
                <i class="menu-icon fa fa-star-o "></i>
                <strong>ADMIN</strong>

                <b class="arrow"></b>
              </a>
            </li>

            <li class="">
              <a href="edit_users.php?id=99999">
                <i class="menu-icon fa fa-plus"></i>
                Novo Usuário

                <b class="arrow"></b>
              </a>
            </li>

            <?php

$objeto = new front_usuarios;

// var_dump($objeto->todos); //DEBUG

foreach ($objeto->todos as $key => $value) {
  if ($key == 0)
  {
  $key = 1;
  continue;
  }
    $menu_item = '<li class="">
                    <a href="edit_users.php?id='.$value['id_usuario'].'">
                      <i class="menu-icon fa fa-caret-right"></i>';

    $menu_item .= $value["user"].'<br>';
    $menu_item .= '</a>

    <b class="arrow"></b>
  </li>';
  echo $menu_item;
}


 ?>
            </ul>
          </li>
<!-- FIM DO ITEM Usuarios -->
<!-- Inicio de IDIOMAS -->
          <li>
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa  fa-flag "></i>
              <span class="menu-text"> Idiomas </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                    <?php

$objeto = new front_idiomas;

// var_dump($objeto->todos); //DEBUG

foreach ($objeto->todos as $key => $value) {

    $menu_item = '<li class="">
                    <a href="edit_langs.php?id='.$value['id_idioma'].'">
                      <i class="menu-icon fa fa-caret-right"></i>';

    $menu_item .= $value["nome"].'<br>';
    $menu_item .= '</a>

    <b class="arrow"></b>
  </li>';
  echo $menu_item;
}


 ?>
            </ul>
          </li>
<!-- FIM DO ITEM Usuarios -->
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>
      <div class="main-content">
<div class="main-content-inner">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
  <ul class="breadcrumb">
  <li>
  <i class="ace-icon fa fa-home home-icon"></i>
  <a href="#">Administração</a>
  </li>
  <li class="active"><?php if(isset($title_breadcrumb)) echo $title_breadcrumb; ?></li>
  </ul><!-- /.breadcrumb -->
  </div>

<div class="page-content">

<div class="page-header">
<h1>
<?php if(isset($title_breadcrumb)) echo $title_breadcrumb; ?>
</h1>
</div><!-- /.page-header -->

<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->

<div class="row">
<div class="space-6"></div>
<div>
