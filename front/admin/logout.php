<?php
session_start();

session_unset();
session_destroy();// MUITA DESTRUICAO NE

$_SESSION = '';

 ?>

 <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>.::admin::.</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <style>
    body {
      background: #eee !important;
    }

    .wrapper {
      margin-top: 80px;
      margin-bottom: 80px;
    }

    .form-signin {
    max-width: 330px;
    padding: 15px 35px 45px;
    margin: 0 auto;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,0.1);

    .form-signin-heading,
    .checkbox {
      margin-bottom: 30px;
    }

    .checkbox {
      font-weight: normal;
    }

    .form-control {
      position: relative;
      font-size: 16px;
      height: 40px;
      padding: 10px;
      @include box-sizing(border-box);

      &:focus {
        z-index: 2;
      }
    }

    input[type="text"] {
      margin-bottom: -1px;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
    }

    input[type="password"] {
      margin-bottom: 20px;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    }

    </style>
  </head>
  <body>
    <div class="wrapper">
    	<p class="alert alert-danger container">Logout realizado com sucesso!</p>
      <img style="width: 170px; margin-bottom: 30px;" class="center-block img-responsive" src="http://sa2.com.br/daray/wp-content/uploads/2015/11/logodaray.png">
      <?php
      if(isset($_SESSION["msg_login"]))
      {
      echo   '<div class="alert alert-danger"><strong>'.$_SESSION["msg_login"]. '</strong></div>';  //ALBERTO CUSTOMIZA ISSO COM ALERTA E tal
      }
      ?>
      <form class="form-signin" action="include/login_back.php" method="POST" style="padding-top: 30px; border-radius: 5px;">
        <span>Nome de usuário</span>
        <input style="border-radius: 0;" type="text" class="form-control" name="username" placeholder="E-mail" required autofocus/><br>
        <span>Senha</span>
        <input style="border-radius: 0;" type="password" class="form-control" name="password" placeholder="●●●●●●●" required/><br><br>
        <button style="border-radius: 0; padding: 5px 40px" class="btn btn-primary center-block" type="submit">Login</button>
      </form>
    </div>
  </body>
</html>
