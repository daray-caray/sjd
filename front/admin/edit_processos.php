<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Processos";
$script = '';

include "header.php";

$id_processo = $_GET['id_processo'];

echo "<a class='btn pull-right' href='list_processos.php'>Voltar</a>";
?>

<?php

$visivel = array('', 'id_processo', 'link_id_cliente', 'forum', 'partes', 'vara', 'numgde', 'controle', 'tipo', 'obs', 'pendente', 'motivo_pendente', 'ano', 'reu_ou_ator', 'titulo', 'recurso', 'distribuicao_data', 'ativo', 'situacao', 'senha_web');  // ALBERTO aqui a gente deixa ele ver o que a gente quiser. O PRIMEIRO SAI CAGADO, eu já vi isso em algum lugar, mas sem tempo de olhar o pq novamente.

$labels = array('' => '',
'id_processo'  => '',
'link_id_cliente'   => '',
'forum'  => 'Fórum',
'partes'  => 'Partes',
'vara'  => 'Vara',
'numgde'  => 'Número de identificação do processo',
'controle' => 'Controle',
'tipo' => 'Tipo',
'obs' => 'Observações',
'pendente' => 'Pendente',
'motivo_pendente' => 'Motivo Pendente',
'ano' => 'Ano',
'reu_ou_ator' => 'Réu ou Ator',
'titulo' => 'Título',
'recurso' => 'Recurso',
'distribuicao_data' => 'Distribuição data',
'ativo' => 'Ativo',
'situacao' => 'Situação',
'senha_web' => 'Senha web',
);

$types = array('' => '',
'id_processo'  => 'hidden',
'link_id_cliente'   => 'hidden',
'forum'  => 'text',
'partes'  => 'text',
'vara'  => 'text',
'numgde'  => 'number',
'controle' => 'text',
'tipo' => 'text',
'obs' => 'textarea',
'pendente' => 'checkbox',
'motivo_pendente' => 'text',
'ano' => 'text',
'reu_ou_ator' => 'text',
'titulo' => 'text',
'recurso' => 'text',
'distribuicao_data' => 'date',
'ativo' => 'checkbox',
'situacao' => 'text',
'senha_web' => 'password',
);

$objeto = new processos;
$objeto_array = $objeto->seleciona($id_processo);

$values = array('' => '',
'id_processo'=>$objeto_array['id_processo'],
'link_id_cliente' =>$objeto_array['link_id_cliente'],
'forum'  =>$objeto_array['forum'],
'partes'  =>$objeto_array['partes'],
'vara'  =>$objeto_array['vara'],
'numgde'  =>$objeto_array['numgde'],
'controle' =>$objeto_array['controle'],
'tipo' =>$objeto_array['tipo'],
'obs' =>$objeto_array['obs'],
'pendente' =>$objeto_array['pendente'],
'motivo_pendente' =>$objeto_array['motivo_pendente'],
'ano' =>$objeto_array['ano'],
'reu_ou_autor' =>$objeto_array['reu_ou_autor'],
'titulo' =>$objeto_array['titulo'],
'recurso' =>$objeto_array['recurso'],
'distribuicao_data' =>$objeto_array['distribuicao_data'],
'ativo' =>$objeto_array['ativo'],
'situacao' =>$objeto_array['situacao'],
'senha_web' =>$objeto_array['senha_web'],
);

$action = 'include/ops_edit_processos.php';
montaForm('processos',$visivel,$labels,$types,$action, $values);

include "footer.php";


?>
