<?php
session_start();
$database = "sjd_front.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$script = '';
// include "include/ops_edit.php";

$lid = $_GET["lid"];
$id = $_GET["id"];

if(!isset($lid)) exit("Não foi possivel selecionar o idioma.");
if(!isset($id)) exit("Não foi possivel selecionar o Conteúdo.");

if($_GET["id"] != '99999'){
$objeto = new front_conteudo;
$pag = $objeto->seleciona($_GET["id"]);

$_var_title = "title_idioma_$lid";
$_var_conteudo = "conteudo_idioma_$lid";


$titulo = $pag["$_var_title"];
$title_breadcrumb = $pag["$_var_title"];
$conteudo_pag = $pag["$_var_conteudo"];
}
if($_GET["id"] == '99999'){
$titulo = '';
$title_breadcrumb = 'Nova página';
$conteudo_pag = '';
}
include "header.php";


// var_dump($_GET); //DEBUG
?>

<?php if($_GET["id"] != '99999' && $_GET["id"] != '0' ) : ?>

<a href="../conteudo.php?page=<?php echo $_GET["id"]; ?>" target=_blank> VISUALIZAR PÁGINA </a>

<form action="include/excluir_pagina.php" method="get" onSubmit="return confirm('Você realmente quer deletar essa página?')">
	<input type="hidden" name="id_pagina" value="<?php echo $_GET["id"]; ?>">
	<button type="submit" class="btn pull-right btn-danger">DELETAR PÁGINA</button>
</form>
<?php endif; ?>

<?php if(isset($retorno_update)) : ?>
<div class="col-md-12">
	<?php echo $retorno_update; ?>
</div>
<?php endif; ?>

<form enctype="multipart/form-data" action="include/ops_edit.php" method="post">
	<div class="col-md-12">
		<div class="control-group">
			<label class="control-label bolder blue">Titulo (Requerido)</label><br>
			<input type="text" class="col-md-6" placeholder="Titulo" required name="titulo" value="<?php echo $titulo; ?>">
			<input type="hidden" name="id_conteudo" value="<?php echo $_GET["id"]; ?>">
			<input type="hidden" name="lid" value="<?php echo $lid; ?>">
		</div>
		<br><br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Imagem de capa da página</label><br>
			<input type="file" name="arquivo" class="col-md-6">
		</div>
		<br><br><br>

		<div class="col-md-8">
			<textarea style="height: 300px;" name="conteudo"><?php echo $pag["$_var_conteudo"]; ?></textarea>
		</div>

		<div class="col-md-12" style="margin-top: 10px;">
			<button class="btn" type="submit">Salvar</button>
		</div>
	</div>
</form>


<?php include "footer.php"; ?>
