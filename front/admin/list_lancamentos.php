<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Lancamentos";

include "header.php";

echo '<a class="btn btn-success pull-right" href="add_lancamentos.php">Novo lançamento</a><br><br><br><br>';

$visivel = array('', 'id_lancamento', 'link_id_cliente', 'vencimento', 'tipo', 'vlrdoc', 'baixado');

montaTabela('lancamentos',$visivel);

include "footer.php";

?>
