<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Processos";

include "header.php";

echo "<a class='btn pull-right' href='list_processos.php'>Voltar</a>";

$visivel = array('','nome','rg','telefone','end','num','email');  // ALBERTO aqui a gente deixa ele ver o que a gente quiser. O PRIMEIRO SAI CAGADO, eu já vi isso em algum lugar, mas sem tempo de olhar o pq novamente.
$labels = array('' => '',
'nome'  => 'Cliente (Nome)',
'rg'   => 'RG',
'telefone'  => 'Telefone Fixo',
'end'  => 'Endereço',
'num'  => 'Número',
'email'  => 'E-mail'
);
$types = array('' => '',
'nome'  => 'text',
'rg'   => 'number',
'telefone'  => 'number',
'end'  => 'text',
'num'  => 'text',
'email'  => 'email'
);
$action = 'include/ops_add_processos.php';
montaForm('clientes',$visivel,$labels,$types,$action);

include "footer.php";

//$visivel = array('','link_id_cliente','forum','partes','vara','controle','pendente', 'data');

?>
