<?php
session_start();
$database = "sjd_front.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$script = '';
// include "include/ops_edit.php";

if($_GET["id"] != '99999'){
$objeto = new front_usuarios;
$pag = $objeto->seleciona($_GET["id"]);

$nome = $pag["nome"];
$email = $pag["email"];
$user = $pag["user"];
$senha = $pag["hash"];

}
if($_GET["id"] == '99999'){
$titulo = '';
$title_breadcrumb = 'Novo Usuário';
$conteudo_pag = '';
}
include "header.php";


// var_dump($_GET); //DEBUG
?>

<?php if($_GET["id"] != '99999' && $_GET["id"] != '0' ) : ?>
<form action="include/excluir_user.php" method="get" onSubmit="return confirm('Você realmente quer deletar esse usuario?')">
	<input type="hidden" name="id_usuario" value="<?php echo $_GET["id"]; ?>">
	<button type="submit" class="btn pull-right btn-danger">DELETAR USUARIO</button>
</form>
<?php endif; ?>

<?php if(isset($retorno_update)) : ?>
<div class="col-md-12">
	<?php echo $retorno_update; ?>
</div>
<?php endif; ?>

<form enctype="multipart/form-data" action="include/ops_edit_user.php" method="post">
	<input type="hidden" name="id_usuario" value="<?php echo $_GET["id"]; ?>">
	<div class="col-md-12">

		<div class="control-group">
			<label class="control-label bolder blue">Nome</label><br>
			<input type="text" class="col-md-6" placeholder="Nome" required name="nome" value="<?php echo $nome; ?>">
		</div>
		<br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Email</label><br>
			<input type="text" class="col-md-6" placeholder="exemplo@exemplo.com" required name="email" value="<?php echo $email; ?>">
		</div>
		<br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">User</label><br>
			<input type="text" class="col-md-6" placeholder="Username" required name="user" value="<?php echo $user; ?>">
		</div>
		<br><br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Senha</label><br>
			<input type="password" class="col-md-6" required name="senha">
		</div>
		<br><br><br>


		<div class="col-md-12" style="margin-top: 10px;">
			<button class="btn" type="submit">Salvar</button>
		</div>
	</div>
</form>


<?php include "footer.php"; ?>
