<?php
session_start();
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$script = '';
// include "include/ops_edit.php";

$objeto = new front_idiomas;
$pag = $objeto->seleciona($_GET["id"]);
$nome = $pag["nome"];
$id = $_GET["id"];


include "header.php";


// var_dump($_GET); //DEBUG
?>

<?php if(isset($retorno_update)) : ?>
<div class="col-md-12">
	<?php echo $retorno_update; ?>
</div>
<?php endif; ?>

<form enctype="multipart/form-data" action="include/ops_edit_lang.php" method="post">
	<input type="hidden" name="id_idioma" value="<?php echo $_GET["id"]; ?>">
	<div class="col-md-12">

		<div class="control-group">
			<label class="control-label bolder blue">ID</label><br>
			<input type="text" class="col-md-6" placeholder="id" required name="id" value="<?php echo $id; ?>" disabled>
		</div>
		<br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Nome</label><br>
			<input type="text" class="col-md-6" placeholder="Português" required name="nome" value="<?php echo $nome; ?>">
		</div>
		<br><br><br>

		<div class="col-md-12" style="margin-top: 10px;">
			<button class="btn" type="submit">Salvar</button>
		</div>
	</div>
</form>


<?php include "footer.php"; ?>
