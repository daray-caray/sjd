<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Andamentos";
$script = '';

include "header.php";

echo "<a class='btn pull-right' href='list_andamentos.php'>Voltar</a>";

$visivel = array('', 'id_andamento', 'link_id_processo', 'titulo_andamento', 'corpo_andamento');

$labels = array('' => '',
'id_andamento'  => '',
'link_id_processo'   => 'link_id_processo',
'titulo_andamento'  => 'Título',
'corpo_andamento'  => 'Conteúdo Andamento',
);

$types = array('' => '',
'id_andamento'  => 'hidden',
'link_id_processo'   => 'number',
'titulo_andamento'  => 'text',
'corpo_andamento'  => 'textarea',
);

$action = 'ACTION_COLOCAR';
montaForm('andamentos',$visivel,$labels,$types,$action);

include "footer.php";

?>
