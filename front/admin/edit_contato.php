<?php
ini_set("display_errors", true);
error_reporting(E_ALL);
session_start();
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$script = '';
// include "include/ops_edit.php";
$id =  $_GET["id"];
$json = json_decode(file_get_contents('include/dados_contato.json'),true);

// var_dump($json);
// exit;

$nome_label   = $json[$id]["nome_label"];
$email_label = $json[$id]["email_label"];
$site_label = $json[$id]["site_label"];
$comment_label =$json[$id]["comment_label"];


include "header.php";


// var_dump($_GET); //DEBUG
?>

<?php if(isset($retorno_update)) : ?>
<div class="col-md-12">
	<?php echo $retorno_update; ?>
</div>
<?php endif; ?>

<form enctype="multipart/form-data" action="include/ops_edit_contato.php" method="post">
	<input type="hidden" name="id_idioma" value="<?php echo $_GET["id"]; ?>">
	<div class="col-md-12">

		<div class="control-group">
			<label class="control-label bolder blue">Nome</label><br>
			<input type="text" class="col-md-6" placeholder="Português" required name="nome" value="<?php echo $nome_label; ?>">
		</div>
		<br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Email</label><br>
			<input type="text" class="col-md-6" placeholder="asdf@asdf.com" required name="email" value="<?php echo $email_label; ?>">
		</div>
		<br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Site</label><br>
			<input type="text" class="col-md-6" placeholder="asdf.com" required name="site" value="<?php echo $site_label; ?>">
		</div>
		<br><br><br>

		<div class="control-group">
			<label class="control-label bolder blue">Comentário</label><br>
			<input type="text" class="col-md-6" placeholder="comment" required name="comment" value="<?php echo $comment_label; ?>">
		</div>
		<br><br><br>

		<div class="col-md-12" style="margin-top: 10px;">
			<button class="btn" type="submit">Salvar</button>
		</div>
	</div>
</form>


<?php include "footer.php"; ?>
