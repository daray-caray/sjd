<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Clientes";
$script = '';

$id_cliente = $_GET['id_cliente'];

include "header.php";

$objeto = new clientes;
$objeto_array = $objeto->seleciona($id_cliente);

echo "<a class='btn pull-right' href='list_clientes.php'>Voltar</a>";

$visivel = array('', 'id_cliente', 'nome', 'rg', 'cpfcnpj', 'telefone', 'celular', 'telcom', 'log', 'end', 'num', 'comp', 'bairro', 'cidade', 'cep', 'uf', 'obs', 'ativo', 'email', 'tipo');  // ALBERTO aqui a gente deixa ele ver o que a gente quiser. O PRIMEIRO SAI CAGADO, eu já vi isso em algum lugar, mas sem tempo de olhar o pq novamente.

$labels = array('' => '',
'id_cliente'  => '',
'nome'   => 'Nome',
'rg'  => 'RG',
'cpfcnpj'  => 'Cpf / Cnpj',
'telefone'  => 'Telefone',
'celular'  => 'Celular',
'telcom' => 'Telefone Comercial',
'log' => 'log',
'end' => 'Endereço',
'num' => 'Número',
'comp' => 'Complemento',
'bairro' => 'Bairro',
'cidade' => 'Cidade',
'cep' => 'CEP',
'uf' => 'UF',
'obs' => 'obs',
'ativo' => 'Ativo',
'email' => 'E-mail',
'tipo' => 'Tipo',
);

$types = array('' => '',
'id_cliente'  => 'hidden',
'nome'   => 'text',
'rg'  => 'number',
'cpfcnpj'  => 'number',
'telefone'  => 'number',
'celular'  => 'number',
'telcom' => 'number',
'log' => 'text',
'end' => 'text',
'num' => 'number',
'comp' => 'text',
'bairro' => 'text',
'cidade' => 'text',
'cep' => 'number',
'uf' => 'text',
'obs' => 'textarea',
'ativo' => 'checkbox',
'email' => 'email',
'tipo' => 'text',
);

$objeto = new clientes;
$objeto_array = $objeto->seleciona($id_cliente);

$values = array('' => '',
'id_cliente'=> $objeto_array['id_cliente'],
'nome'=> $objeto_array['nome'],
'rg'=> $objeto_array['rg'],
'cpfcnpj'=> $objeto_array['cpfcnpj'],
'telefone'=> $objeto_array['telefone'],
'celular'=> $objeto_array['celular'],
'telcom'=> $objeto_array['telcom'],
'log'=> $objeto_array['log'],
'end'=> $objeto_array['end'],
'num'=> $objeto_array['num'],
'comp'=> $objeto_array['comp'],
'bairro'=> $objeto_array['bairro'],
'cidade'=> $objeto_array['cidade'],
'cep'=> $objeto_array['cep'],
'uf'=> $objeto_array['uf'],
'obs'=> $objeto_array['obs'],
'ativo'=> $objeto_array['ativo'],
'email'=> $objeto_array['email'],
'tipo'=> $objeto_array['tipo'],
);

$action = 'ACTION_COLOCAR';
montaForm('clientes',$visivel,$labels,$types,$action, $values);

include "footer.php";

?>
