<?php
session_start();
$database = "sjd_main.db";
$login_needed = 1;
$path = '../../';
include $path.'include/include_all.php';
$title_breadcrumb = "Novo cliente";
$script = '';

include "header.php";

echo "<a class='btn pull-right' href='list_clientes.php'>Voltar</a>";

$visivel = array('', 'id_cliente', 'nome', 'rg', 'cpfcnpj', 'telefone', 'celular', 'telcom', 'log', 'end', 'num', 'comp', 'bairro', 'cidade', 'cep', 'uf', 'obs', 'ativo', 'email', 'tipo');  // ALBERTO aqui a gente deixa ele ver o que a gente quiser. O PRIMEIRO SAI CAGADO, eu já vi isso em algum lugar, mas sem tempo de olhar o pq novamente.

$labels = array('' => '',
'id_cliente'  => '',
'nome'   => 'Nome',
'rg'  => 'Fórum',
'cpfcnpj'  => 'Cpf / Cnpj',
'telefone'  => 'Telefone',
'celular'  => 'Celular',
'telcom' => 'Telefone Comercial',
'log' => 'log',
'end' => 'Endereço',
'num' => 'Número',
'comp' => 'Complemento',
'bairro' => 'Bairro',
'cidade' => 'Cidade',
'cep' => 'CEP',
'uf' => 'UF',
'obs' => 'obs',
'ativo' => 'Ativo',
'email' => 'E-mail',
'tipo' => 'Tipo',
);

$types = array('' => '',
'id_cliente'  => 'hidden',
'nome'   => 'text',
'rg'  => 'number',
'cpfcnpj'  => 'number',
'telefone'  => 'number',
'celular'  => 'number',
'telcom' => 'number',
'log' => 'text',
'end' => 'text',
'num' => 'number',
'comp' => 'text',
'bairro' => 'text',
'cidade' => 'text',
'cep' => 'number',
'uf' => 'text',
'obs' => 'textarea',
'ativo' => 'checkbox',
'email' => 'email',
'tipo' => 'text',
);

$action = 'ACTION_COLOCAR';
montaForm('clientes',$visivel,$labels,$types,$action);

include "footer.php";

?>
