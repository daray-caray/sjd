<?php
$page = $_GET['page'];

if(!isset($page)){
  exit("ERRO AO SELECIONAR O CONTEUDO");
}


session_start();
$path = '../';
include $path."include/include_all.php";
include "header.php";



$objeto = new front_conteudo;

$value = $objeto->seleciona($page);
// // $dados_page = $objeto->todos;
// echo 'teste';
//  var_dump($dados_page);
// exit;


if($_SESSION['lingua'] == '3')
{
$title = $value["title_idioma_3"];
$content = $value["conteudo_idioma_3"];
}
elseif($_SESSION['lingua'] == '2')
{
$title = $value["title_idioma_2"];
$content = $value["conteudo_idioma_2"];
}
else
{
$title = $value["title_idioma_1"];
$content = $value["conteudo_idioma_1"];
}

 ?>

<div id="main" class="site-main">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<article id="post-27" class="post-27 post type-post status-publish format-standard has-post-thumbnail hentry category-direito-do-trabalho tag-advogado tag-direito-civil tag-direito-da-familia tag-direito-das-sucessoes tag-direito-do-trabalho tag-sao-roque">


				<div class="post-thumbnail">
					<img src="<?php echo $objeto->dados->img_titulo ?>" class="attachment-twentyfourteen-full-width size-twentyfourteen-full-width wp-post-image" alt="adv4" />
				</div>


				<header class="entry-header">
					<h1 class="entry-title"><?php echo $title; ?></h1>
				</header>

				<div class="entry-content">
					<p>
						<?php echo $content; ?>
					</p>

					<div id='jp-relatedposts' class='jp-relatedposts' >
						<h3 class="jp-relatedposts-headline"><em><?php echo $label_related ?></em></h3>
					</div>
				</div>
			</article>
			<nav class="navigation post-navigation" role="navigation">
				<h1 class="screen-reader-text">Navega��o do post</h1>
			</nav>
		</div>
	</div>

</div>


<?php include "footer.php"; ?>
