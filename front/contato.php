<?php
session_start();
$path = '../';
include $path."include/include_all.php";
include "header.php";
?>

<div id="main" class="site-main">
	<div id="main-content" class="main-content">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<article id="post-42" class="post-42 page type-page status-publish hentry">
					<header class="entry-header"><h1 class="entry-title">Contato</h1></header>
					<div class="entry-content">
						<div id='contact-form-42'>
							<form action='#' method='post' class='contact-form commentsblock'>

								<div>
									<label for='g42-nome' class='grunion-field-label name'>Nome<span>(obrigatório)</span></label>
									<input type='text' name='g42-nome' id='g42-nome' value='' class='name'  required aria-required='true'/>
								</div>

								<div>
									<label for='g42-email' class='grunion-field-label email'>Email<span>(obrigatório)</span></label>
									<input type='email' name='g42-email' id='g42-email' value='' class='email'  required aria-required='true'/>
								</div>

								<div>
									<label for='g42-site' class='grunion-field-label url'>Site</label>
									<input type='text' name='g42-site' id='g42-site' value='' class='url'  />
								</div>

								<div>
									<label for='contact-form-comment-g42-comentrio' class='grunion-field-label textarea'>Comentário<span>(obrigatório)</span></label>
									<textarea name='g42-comentrio' id='contact-form-comment-g42-comentrio' rows='20' class='textarea'  required aria-required='true'></textarea>
								</div>
								<p class='contact-submit'>
									<input type='submit' value='Enviar &#187;' class='pushbutton-wide'/>
									<input type='hidden' name='contact-form-id' value='42' />
									<input type='hidden' name='action' value='grunion-contact-form' />
								</p>
							</form>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>

	<div id="secondary">
		<h2 class="site-description">Registro nº 12.345</h2>
	</div>

</div>

<?php include "footer.php"; ?>
