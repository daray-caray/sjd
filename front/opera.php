<?php
ini_set('display_errors',true);
error_reporting(E_ALL);
$path = '../';
include $path."include/include_all.php";

session_start();

//if(!empty($_SERVER['HTTP_REFERER'])) $ref = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], '/') + 1);
$ref = 'index.php'; //Para mudar a lingua de qualquer lugar do site e voltar para o mesmo!

if(!isset($_POST['lingua']) && !isset($_SESSION['lingua']) )
{
  $_SESSION["lingua"] = '1';
  redirect($ref);
}

elseif(isset($_POST["lingua"]))
{
  $_SESSION["lingua"] = $_POST['lingua'];
  redirect($ref);
}


?>
