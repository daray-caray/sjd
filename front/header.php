<?php
$path = '../';
include $path.'include/include_all.php';
$objeto_idiomas = new front_idiomas;
?>
<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>SJD</title>
	<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
	</style>
	<link rel='stylesheet'  href='https://fonts.googleapis.comcss?family=Lato%3A300%2C400%2C700%2C900%2C300italic%2C400italic%2C700italic&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />

<link rel='stylesheet' id='twentyfourteen-jetpack-css'  href='css/style_1.css' type='text/css' media='all' />
<link rel='stylesheet' id='genericons-css'  href='css/genericon.css' type='text/css' media='all' />
<link rel='stylesheet' id='twentyfourteen-style-css'  href='css/style_2.css' type='text/css' media='all' />
<link rel='stylesheet' id='jetpack_image_widget-css'  href='css/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='jetpack_css-css'  href='css/jetpack.css' type='text/css' media='all' />
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-migrate.js'></script>

<style type='text/css'>img#wpstats{display:none}</style>	<style type="text/css" id="twentyfourteen-header-css">
			.site-title,
		.site-description {
			clip: rect(1px 1px 1px 1px);
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute;
		}
		</style>
	<style type="text/css" id="custom-background-css">
body.custom-background { background-color: #000000; background-image: url('img/fundofalso.png'); background-repeat: repeat; background-position: top left; background-attachment: scroll; }
</style>

<style id="custom-css-css">.entry-meta{display:none}.copyright{display:none}.site-info{display:none}.jp-relatedposts-post-date{display:none}</style>
</head>

<body class="home page page-id-2 page-template-default custom-background header-image full-width footer-widgets slider">

	<form style="padding: 10px 30px 30px" id="form1" name="form1" method="post" action="opera.php">
		<label style="color: #fff;" for="lingua">Idioma:</label><br>
		<select name="lingua" id="lingua" onchange='if(this.value != 0) { this.form.submit(); }'>
			<option value="null">----</option>
			<?php echo $objeto_idiomas->seleciona(1)['nome']; ?>
			<option value="1"><?php echo $objeto_idiomas->seleciona(1)['nome']; ?></option>
			<option value="2"><?php echo $objeto_idiomas->seleciona(2)['nome']; ?></option>
			<option value="3"><?php echo $objeto_idiomas->seleciona(3)['nome']; ?></option>
		</select>
		<br class="clear" />
	</form>

<div id="page" class="hfeed site">
		<div id="site-header" style="background-color: #000;">
		<a href="index.php" rel="home">
			<img src="img/sjd.png" width="300" alt="Logo do cliente">
		</a>
	</div>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-main">
			<h1 class="site-title"><a href="\front\index.php" rel="home">Sistema Juridico Daray</a></h1>

			<div class="search-toggle">
				<a href="#search-container" class="screen-reader-text" aria-expanded="false" aria-controls="search-container">Pesquisa</a>
			</div>

			<?php include 'menu.php'; ?>

		</div>

		<div id="search-container" class="search-box-wrapper hide">
			<div class="search-box">
				<form role="search" method="get" class="search-form" action="index.php">
				<label>
					<span class="screen-reader-text">Pesquisar por:</span>
					<input type="search" class="search-field" placeholder="Pesquisar &hellip;" value="" name="s" title="Pesquisar por:" />
				</label>
				<input type="submit" class="search-submit" value="Pesquisar" />
			</form>			</div>
		</div>
	</header><!-- #masthead -->
