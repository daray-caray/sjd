# SJD #

Site com admin para advogados

  - Alberto Aguiar Neto
  - Davino Gonçalves Junior
  - Yuri Sá  [@yurisa2](https://about.me/yuri.sa)
(2650831523026)

# Básico de Operação #

O sistema todo manipula o Banco de Dados através de um objeto criado a partir do PDO para TODAS as operações System-Wide.

### Como funciona o objeto? ###

Cada tabela tem um objeto para manipular ela. Eles estão listados em include/crud_especifico.php
Eles estendem um objeto principal que efetivamente contem as funções e as properties.

Basta instanciar para que as properties subam:

#### Objeto principal: ####

$objeto = new nome_do_objeto_estendido;

##### Lista todos:  #####

$objeto->todos;

Assim que você instancia o objeto essa property é preenchida com um SELECT *

##### Selecionar um registro somente: #####
$objeto->seleciona($id);

Essa Interface é dual, ou seja, ele te RETORNA UM ARRAY ASSOC, porém ele joga os dados em

$objeto->dados também (aquela mesma do update/insere)...

##### Deletar um registro:   #####
$objeto->deleta($id); //Simples assim

##### Inserir: #####
Insira dos dados na property dados->property (cada property deve ter o nome exato da coluna da tabela):  

$objeto->dados->id_conteudo = "TITULO";

$objeto->dados->title_idioma_1 = "TITULO";

$objeto->dados->conteudo_idioma_1 = "TITULO";

$objeto->insere(); //Ele vai chamar os dados na property $objeto->dados e inserir tudo

##### Atualiza: #####
Funciona do mesmo modo do inserir, colocando as alterações em $objeto->dados e depois

$objeto->atualiza($id); //Aqui ele pede o ID por motivos óbvios