<?php include_once "include/include_all.php"; ?>
<?php include_once "alberto.php"; ?>
<?php require_once "include/front/header.php"; ?>

<?php
$objeto = new clientes;
?>
<div class="container">
  <h2>Teste tabela</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <?php foreach ($objeto->colunas as $colunas) : ?>
            <th><?php echo strtoupper($colunas["name"]); ?></th>
        <?php endforeach; ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($objeto->todos as $dados) : ?>
      <tr>
        <?php foreach ($dados as $key => $value) : ?>
        <td><?php echo $value; ?></td>
      <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</div>

<?php
echo "<pre>";
var_dump($objeto);
?>
<?php require_once "include/front/footer.php"; ?>
