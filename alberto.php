<?php
ini_set("display_errors", true);
$path = '';
$database = 'sjd_main.db';
include_once $path."include/include_all.php";
include_once $path."include/front/header.php";

function montaTabela() {
  $objeto = new lancamentos;

  $tabela = "
  <div class='container'>
    <h2>Teste tabela</h2>
    <table class='table table-striped'>
      <thead>
        <tr>
        ";

  foreach ($objeto->colunas as $colunas) {
    $tabela .= "<th>".strtoupper($colunas['name'])."</th>";
  }

  $tabela .= "
    </tr>
      </thead>
        <tbody>
        ";

  foreach ($objeto->todos as $dados) {
    $tabela .= "<tr>";

    foreach ($dados as $key => $value) {
      $tabela .= "<td>".$value."</td>";
    }

    $tabela .= "</tr>";
  }

  $tabela .= "
    </tbody>
  </table>
</div>
";

return $tabela;
}

var_dump(montaTabela());

?>
